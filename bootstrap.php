<?php
use R3C\Wordpress\EventDispatcher\EventDispatcher;
use R3C\Wordpress\Twig\TwigFactory;

EventDispatcher::addListener(function() {
    add_filter('template_include', 'r3cWordpressTwigLoadTemplate', 1);
});

function r3cWordpressTwigLoadTemplate($templateFile) {
    global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

    if ( is_array( $wp_query->query_vars ) )
        extract( $wp_query->query_vars, EXTR_SKIP );

    $path_parts = pathinfo($templateFile);
    $dirname = $path_parts['dirname'];
    $filename = $path_parts['basename'];
    $twigFile = $dirname . '/' . $filename . '.twig';

    if (file_exists($twigFile)) {
        $variables = [
            'posts' => $posts,
            'post' => $post,
            'wp_did_header' => $wp_did_header,
            'wp_query' => $wp_query,
            'wp_rewrite' => $wp_rewrite,
            'wpdb' => $wpdb,
            'wp_version' => $wp_version,
            'wp' => $wp,
            'id' => $id,
            'comment' => $comment,
            'user_ID' => $user_ID
        ];

        echo TwigFactory::render($twigFile, $variables);
        return '';
    }

    return $templateFile;
}

?>