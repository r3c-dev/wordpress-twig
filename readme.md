## R3C Wordpress Twig

Pacote para integração do Twig com o Wordpress


//Mágica

Automaticamente, qualquer arquivo do tema que tiver o nome com .twig no final, será renderizado pelo twig. Por exemplo, se existir um header.php.twig, ele será o renderizado ao invés do header.php


//Mapeia a pasta com as views



```
#!php
use R3C\Wordpress\EventDispatcher\EventDispatcher;
use R3C\Wordpress\Twig\TwigFactory;


EventDispatcher::addListener(function() {

	TwigFactory::addViewPath('/path/ate/a/pasta/com/views', 'namespace');
	
});

```


//Usa a view



```
#!php

echo TwigFactory::render('@namespace/view.twig', [
	'nomeVariavel' => $variavel
]);
```