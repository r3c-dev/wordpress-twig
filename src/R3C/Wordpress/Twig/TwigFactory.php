<?php
namespace R3C\Wordpress\Twig;

use Twig_Loader_Filesystem;
use Twig_Environment;

class TwigFactory
{
    public static $extensions;
    public static $loader;
    public static $twig;
    public static $viewsPath;

    public static function addViewPath($path, $namespace = 'main')
    {
        if (self::$viewsPath == null) {
            self::$viewsPath = [
                get_template_directory() => 'main'
            ];
        }

        self::$viewsPath[$path] = $namespace;

        if (self::$twig != null) {
            self::$loader->addPath($path, $namespace);
        }
    }

	public static function render($view, $variables = [])
	{
        if (self::$twig == null) {
    		self::$loader = new Twig_Loader_Filesystem();

            if (self::$viewsPath == null) {
                self::addViewPath(get_template_directory());
            }

            foreach (self::$viewsPath as $path => $namespace) {
                self::$loader->addPath($path, $namespace);
            }

            self::$twig = new Twig_Environment(self::$loader, array(
                'cache' => ABSPATH . '../cache/twig',
                'auto_reload' => true
            ));

            foreach (self::$extensions as $extension) {
                self::$twig->addExtension($extension);
            }
        }

        return self::$twig->render($view, $variables);
	}

    public static function addExtension($extension)
    {
        if (self::$twig != null) {
            self::$twig->addExtension($extension);
        }
        
        self::$extensions[] = $extension;
    }
}